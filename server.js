let express = require("express");
let swagger = require("swagger-node-express");
let swaggerUi = require("swagger-ui-express");
let swaggerJsDoc = require("swagger-jsdoc");

const app = express();
const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "LogRocket Express API with Swagger",
            version: "0.1.0",
            description: "This is a simple CRUD API application made with Express and documented with Swagger",
            license: {
                name: "MIT",
                url: "https://spdx.org/licenses/MIT.html",
            },
            contact: {
                name: "LogRocket",
                url: "https://logrocket.com",
                email: "info@email.com",
            },
        },
        servers: [{
            url: "http://localhost:3000",
        }, ],
    },
    apis: [
        './routes/books.js'
    ],

};



//const specs = swaggerJsdoc(options);


const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

const port = process.env.PORT || 3000;
app.listen(port, () => {

    console.log('Escuchando puerto: ', port);

});